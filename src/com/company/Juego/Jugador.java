package com.company.Juego;

import java.util.ArrayList;
import java.util.LinkedList;

import com.company.herramientas.Posicion;
import com.company.herramientas.SetDePiezas;
import com.company.mundo.Mapa;
import com.company.piezas.pieza;

public class Jugador {

    // constructor
    public Jugador(SetDePiezas piezas, Mapa mapa){
        _piezas = piezas;
        _mapa = mapa;
        if(piezas.get_colorJugador() == "b"){
            _equipo = 1;
        }else{
            _equipo = 2;
        }
    }

    public ArrayList<Posicion> SeleccionarPieza(pieza p){
        return p.ListarPosiblesMovimientos(_mapa, p.equipo());
    }

    public void MoverPieza(pieza pieza, Posicion posicion){
        _mapa.MoverPieza(pieza, posicion);
        pieza.cambiarPosicion(posicion);
    }

    public pieza tienePiezaAhi(Posicion posicion){
        return _mapa.HayPiezaAhi(posicion, this._equipo);
    }

    public boolean llegoAultimaFilaEnemiga(pieza pieza){
        return _mapa.filaContraria(pieza, _filaComienzo);
    }

    public LinkedList<pieza> PiezasVivas(){
        return _piezas.get_piezasVivas();
    }

    public LinkedList<pieza> PiezasMuertas(){
        return _piezas.get_piezasMuertas();
    }

    public void matarPieza(pieza piezaAmatar, pieza piezaAmover){
        _mapa.matarPieza(piezaAmatar, piezaAmover);
        _piezas.MatarPieza(piezaAmatar);
    }

    public void revivirPieza(pieza p, Posicion posicion){
        _piezas.revivirPieza(p, posicion);
        _mapa.revivirPieza(p, posicion);
    }

    public void tablero(){
        _mapa.tablero();
    }

    //* ------------------------------------ *//
    private SetDePiezas _piezas;
    private Mapa _mapa;
    private byte _filaComienzo;
    private byte _equipo;
}
