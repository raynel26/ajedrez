package com.company.Juego;

import com.company.herramientas.Posicion;
import com.company.herramientas.SetDePiezas;
import com.company.mundo.Mapa;
import com.company.piezas.pieza;

import java.util.ArrayList;
import java.util.LinkedList;

public class Juego {

    public Juego(SetDePiezas blancas, SetDePiezas negras) {
        Mapa mapa = new Mapa(blancas, negras);
        Jugador j1 = new Jugador(blancas, mapa);
        Jugador j2 = new Jugador(negras, mapa);
        _blanco = j1;
        _negro = j2;
        _turno = "b";
    }

    public String enTurno(){
        return _turno;
    }

    public ArrayList<Posicion> SeleccionarPiezaa(pieza p){
        if(p.equipo() == 1){
            return _blanco.SeleccionarPieza(p);
        }else {
            return _negro.SeleccionarPieza(p);
        }

    }

    public void MoverBlanco(pieza pieza, Posicion posicion){
        pieza piezaEnemiga = _negro.tienePiezaAhi(posicion);
        if(piezaEnemiga != null){
            _negro.matarPieza(piezaEnemiga, pieza);
        }
        _blanco.MoverPieza(pieza, posicion);
        //if(_blanco.llegoAultimaFilaEnemiga(pieza)){
        //    _blanco.revivirPieza(pieza,posicion);
        //}
    }

    public void MoverNegro(pieza pieza, Posicion posicion){
        pieza piezaEnemiga = _blanco.tienePiezaAhi(posicion);
        if(piezaEnemiga != null){
            _blanco.matarPieza(piezaEnemiga, pieza);
        }
        _negro.MoverPieza(pieza, posicion);
        //if(_negro.llegoAultimaFilaEnemiga(pieza)){
        //    _negro.revivirPieza(pieza, posicion);
        //}
    }

    public void tablero(){
        _blanco.tablero();
    }

    public LinkedList<pieza> piezasBlancasVivas(){
        return _blanco.PiezasVivas();
    }

    public LinkedList<pieza> piezasNegrasVivas(){
        return _negro.PiezasVivas();
    }

    public ArrayList<Posicion> SeleccionarPieza(pieza p){
        if(p.equipo() == 1){
            return _blanco.SeleccionarPieza(p);
        }else{
            return _negro.SeleccionarPieza(p);
        }
    }
   
    public void CambiarTurno(){
        if(_turno == "b"){
            _turno = "n";
        }else{
            _turno = "b";
        }
    }

     //*-------------------------------------------------------------*//
    private Jugador _blanco;
    private Jugador _negro;
    private String _turno;
}
