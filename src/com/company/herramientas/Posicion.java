package com.company.herramientas;

public class Posicion {

    public Posicion(int i, int j){
        x = i;
        y = j;
    }

    @Override
    public String toString() {
        return "Posicion: " + "(" + x + " ,"+ y + ")";
    }

    /*---------------------------------------*/
    public int x, y;
}
