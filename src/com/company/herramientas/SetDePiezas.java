package com.company.herramientas;

import com.company.piezas.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class SetDePiezas {

    public SetDePiezas(String color) {
        _colorJugador = color;
        if (_colorJugador == "b"){
            Torre torre1 = new Torre(new Posicion(0, 0), "torre", (byte)1);
            Torre torre2 = new Torre(new Posicion(7, 0), "torre", (byte)1);
            Alfil alfil1 = new Alfil(new Posicion(2, 0), "alfil", (byte)1);
            Alfil alfil2 = new Alfil(new Posicion(5, 0), "alfil", (byte)1);
            Caballo caballo1 = new Caballo(new Posicion(1, 0), "caballo", (byte)1);
            Caballo caballo2 = new Caballo(new Posicion(6, 0), "caballo", (byte)1);
            Rey rey = new Rey(new Posicion(3, 0), "rey", (byte)1);
            Reina reina = new Reina(new Posicion(4, 0), "reina", (byte)1);

            Peon peon1 = new Peon(new Posicion(0, 1), "peon", (byte)1);
            Peon peon2 = new Peon(new Posicion(1, 1), "peon", (byte)1);
            Peon peon3 = new Peon(new Posicion(2, 1), "peon", (byte)1);
            Peon peon4 = new Peon(new Posicion(3, 1), "peon", (byte)1);
            Peon peon5 = new Peon(new Posicion(4, 1), "peon", (byte)1);
            Peon peon6 = new Peon(new Posicion(5, 1), "peon", (byte)1);
            Peon peon7 = new Peon(new Posicion(6, 1), "peon", (byte)1);
            Peon peon8 = new Peon(new Posicion(7, 1), "peon", (byte)1);

            todasLasPiezas = new HashMap<String, pieza>();
            todasLasPiezas.put("torre1", torre1);
            todasLasPiezas.put("torre2", torre2);
            todasLasPiezas.put("alfil1", alfil1);
            todasLasPiezas.put("alfil2", alfil2);
            todasLasPiezas.put("caballo1", caballo1);
            todasLasPiezas.put("caballo2", caballo2);
            todasLasPiezas.put("reina", reina);
            todasLasPiezas.put("rey", rey);
            todasLasPiezas.put("peon1", peon1);
            todasLasPiezas.put("peon2", peon2);
            todasLasPiezas.put("peon3", peon3);
            todasLasPiezas.put("peon4", peon4);
            todasLasPiezas.put("peon5", peon5);
            todasLasPiezas.put("peon6", peon6);
            todasLasPiezas.put("peon7", peon7);
            todasLasPiezas.put("peon8", peon8);

            _piezasMuertas = new LinkedList<>();
            _piezasVivas = new LinkedList<>();


            _piezasVivas.add(torre1);
            _piezasVivas.add(torre2);
            _piezasVivas.add(alfil1);
            _piezasVivas.add(alfil2);
            _piezasVivas.add(caballo1);
            _piezasVivas.add(caballo2);
            _piezasVivas.add(reina);
            _piezasVivas.add(rey);
            _piezasVivas.add(peon1);
            _piezasVivas.add(peon2);
            _piezasVivas.add(peon3);
            _piezasVivas.add(peon4);
            _piezasVivas.add(peon5);
            _piezasVivas.add(peon6);
            _piezasVivas.add(peon7);
            _piezasVivas.add(peon8);
        }else{
            if (color == "n"){
                Torre torre1 = new Torre(new Posicion(0, 7), "torre", (byte)2);
                Torre torre2 = new Torre(new Posicion(7, 7), "torre", (byte)2);
                Alfil alfil1 = new Alfil(new Posicion(2, 7), "alfil", (byte)2);
                Alfil alfil2 = new Alfil(new Posicion(5, 7), "alfil", (byte)2);
                Caballo caballo1 = new Caballo(new Posicion(1, 7), "caballo", (byte)2);
                Caballo caballo2 = new Caballo(new Posicion(6, 7), "caballo", (byte)2);
                Reina reina = new Reina(new Posicion(3, 7), "reina", (byte)2);
                Rey rey = new Rey(new Posicion(4, 7), "rey", (byte)2);

                Peon peon1 = new Peon(new Posicion(0, 6), "peon", (byte)2);
                Peon peon2 = new Peon(new Posicion(1, 6), "peon", (byte)2);
                Peon peon3 = new Peon(new Posicion(2, 6), "peon", (byte)2);
                Peon peon4 = new Peon(new Posicion(3, 6), "peon", (byte)2);
                Peon peon5 = new Peon(new Posicion(4, 6), "peon", (byte)2);
                Peon peon6 = new Peon(new Posicion(5, 6), "peon", (byte)2);
                Peon peon7 = new Peon(new Posicion(6, 6), "peon", (byte)2);
                Peon peon8 = new Peon(new Posicion(7, 6), "peon", (byte)2);

                todasLasPiezas = new HashMap<String, pieza>();
                todasLasPiezas.put("torre1", torre1);
                todasLasPiezas.put("torre2", torre2);
                todasLasPiezas.put("alfil1", alfil1);
                todasLasPiezas.put("alfil2", alfil2);
                todasLasPiezas.put("caballo1", caballo1);
                todasLasPiezas.put("caballo2", caballo2);
                todasLasPiezas.put("reina", reina);
                todasLasPiezas.put("rey", rey);
                todasLasPiezas.put("peon1", peon1);
                todasLasPiezas.put("peon2", peon2);
                todasLasPiezas.put("peon3", peon3);
                todasLasPiezas.put("peon4", peon4);
                todasLasPiezas.put("peon5", peon5);
                todasLasPiezas.put("peon6", peon6);
                todasLasPiezas.put("peon7", peon7);
                todasLasPiezas.put("peon8", peon8);

                _piezasMuertas = new LinkedList<>();
                _piezasVivas = new LinkedList<>();

                _piezasVivas.add(torre1);
                _piezasVivas.add(torre2);
                _piezasVivas.add(alfil1);
                _piezasVivas.add(alfil2);
                _piezasVivas.add(caballo1);
                _piezasVivas.add(caballo2);
                _piezasVivas.add(reina);
                _piezasVivas.add(rey);
                _piezasVivas.add(peon1);
                _piezasVivas.add(peon2);
                _piezasVivas.add(peon3);
                _piezasVivas.add(peon4);
                _piezasVivas.add(peon5);
                _piezasVivas.add(peon6);
                _piezasVivas.add(peon7);
                _piezasVivas.add(peon8);

            }
        }
    }

    public void MatarPieza(pieza p){
        p.cambiarPosicion(null);
        _piezasMuertas.add(buscarPieza(p, _piezasVivas));
        _piezasVivas.remove(p);
    }

    public void revivirPieza(pieza p, Posicion posicion){
        p.cambiarPosicion(posicion);
        _piezasVivas.add(buscarPieza(p, _piezasMuertas));
        _piezasMuertas.remove(p);
    }

    public LinkedList<pieza> get_piezasVivas(){
        return _piezasVivas;
    }

    public LinkedList<pieza> get_piezasMuertas(){
        return _piezasMuertas;
    }

    public pieza buscarPieza(pieza p, LinkedList<pieza> piezas){
        int i = 0;
        while (i < piezas.size() && piezas.get(i) != p){
            i++;
        }
        return piezas.get(i);
    }

    public String get_colorJugador() {
        return _colorJugador;
    }

    /*-----------------------------------------------------*/
    private LinkedList<pieza> _piezasMuertas;
    private LinkedList<pieza> _piezasVivas;
    HashMap<String, pieza> todasLasPiezas;
    private String _colorJugador;

    public HashMap<String, pieza> piezas() {
        return todasLasPiezas;
    }
}
