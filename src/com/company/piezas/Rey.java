package com.company.piezas;

import com.company.herramientas.Posicion;
import com.company.mundo.Mapa;

import java.util.ArrayList;

public class Rey implements pieza{

    public Rey(Posicion posicion, String nombre, byte id){
        _p = posicion;
        _nombre = nombre;
        _id = id;
    }

    @Override
    public Posicion posicion() {
            return _p;

    }

    @Override
    public ArrayList<Posicion> ListarPosiblesMovimientos(Mapa mapa, byte filacomienzo) {
        ArrayList<Posicion> res = new ArrayList<>();
        AgregarPosicionesDependiendoDireccion(0, res, mapa);
        AgregarPosicionesDependiendoDireccion(1, res, mapa);
        AgregarPosicionesDependiendoDireccion(2, res, mapa);
        AgregarPosicionesDependiendoDireccion(3, res, mapa);
        return res;
    }

    @Override
    public byte equipo() {
        return _id;
    }

    private boolean NoesteFueraDerango(Posicion posicion){
        boolean res = (posicion.y < 8 && posicion.y > -1) && (posicion.x < 8 && posicion.x > -1);
        return res;
    }

    public boolean noHayOtraPiezaAmiga(Posicion posicion, Mapa mapa){
        return mapa.noHayPiezaAmiga(posicion, _id);
    }

    public void AgregarPosicionesDependiendoDireccion(int direccion, ArrayList<Posicion> direcciones, Mapa mapa){

        int i = 1;
        while (i > -1 ){
            Posicion posicion = new Posicion(_p.x, _p.y);
            aumentarEnDireccion(direccion, posicion, i);
            if (NoesteFueraDerango(posicion) && noHayOtraPiezaAmiga(posicion, mapa)){
                direcciones.add(new Posicion(posicion.x, posicion.y));
            }
            i--;
        }
    }

    public void aumentarEnDireccion(int direccion, Posicion posicion, int i){
        if(direccion == 0){
            posicion.x = posicion.x - i;
            posicion.y = posicion.y + 1;
        }else {
            if(direccion == 1){
                posicion.x = posicion.x + 1;
                posicion.y = posicion.y + i;
            }else{
                if(direccion == 2){
                    posicion.x = posicion.x + i;
                    posicion.y = posicion.y - 1;
                }else{
                    posicion.x = posicion.x - 1;
                    posicion.y = posicion.y - i;
                }
            }
        }
    }

    @Override
    public void cambiarPosicion(Posicion posicion) {
        _p = posicion;
    }

    @Override
    public String NombrePieza() {
        return _nombre;
    }
    private String _nombre;

    /*-------------------------------*/
    private Posicion _p;
    byte _id;

}
