package com.company.piezas;

import com.company.herramientas.Posicion;
import com.company.mundo.Mapa;

import java.util.ArrayList;

public class Peon implements pieza{
    public Peon(Posicion posicion, String nombre, byte id){
        _p = posicion;
        _nombre = nombre;
        _id = id;
    }

    @Override
    public Posicion posicion() {
        return _p;
    }

    @Override
    public ArrayList<Posicion> ListarPosiblesMovimientos(Mapa mapa, byte equipo) {
        ArrayList<Posicion> res = new ArrayList<Posicion>();
        int d;
        if (equipo == 1) {
            d = 1;
        } else {
            d = -1;
        }

        Posicion pos = new Posicion(_p.x, _p.y + d);
        int i = 1;
        while (i < 3 && NoesteFueraDerango(pos) && mapa.noHayPiezaAmiga(pos, _id) && !mapa.hayPiezaEnemiga(pos, _id)) {
            res.add(new Posicion(pos.x, pos.y));
            pos.y = pos.y + d;
            i++;
        }
        pos.x = _p.x + d;
        pos.y = _p.y + d;
        if(NoesteFueraDerango(pos) && mapa.hayPiezaEnemiga(pos, _id)){
            res.add(new Posicion(pos.x, pos.y));
        }
        pos.x = _p.x - d;
        pos.y = _p.y + d;
        if(NoesteFueraDerango(pos) && mapa.hayPiezaEnemiga(pos, _id)){
            res.add(new Posicion(pos.x, pos.y));
        }
        return res;
    }

    @Override
    public byte equipo() {
        return _id;
    }

    @Override
    public void cambiarPosicion(Posicion posicion) {
        _p = posicion;
    }

    private boolean NoesteFueraDerango(Posicion posicion){
        boolean res = (posicion.y < 8 && posicion.y > -1) && (posicion.x < 8 && posicion.x > -1);
        return res;
    }

    @Override
    public String NombrePieza() {
        return _nombre;
    }
    private String _nombre;

    /*-------------------------------*/
    private Posicion _p;
    byte _id;
}
