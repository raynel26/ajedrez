package com.company.piezas;

import com.company.herramientas.Posicion;
import com.company.mundo.Mapa;

import java.util.ArrayList;

public class Torre extends Reina{
    public Torre(Posicion posicion, String nombre, byte id) {
        super(posicion, nombre, id);
    }

    @Override
    public ArrayList<Posicion> ListarPosiblesMovimientos(Mapa mapa, byte filacomienzo) {
        ArrayList<Posicion> res = new ArrayList<>();
        AgregarPosicionesDependiendoDireccionEnCruz(0, res, mapa);
        AgregarPosicionesDependiendoDireccionEnCruz(1, res, mapa);
        AgregarPosicionesDependiendoDireccionEnCruz(2, res, mapa);
        AgregarPosicionesDependiendoDireccionEnCruz(3, res, mapa);

        return res;
    }
}
