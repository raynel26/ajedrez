package com.company.piezas;

import com.company.herramientas.Posicion;
import com.company.mundo.Mapa;

import java.util.ArrayList;

public class Alfil extends Reina {

    public Alfil(Posicion posicion, String nombre, byte id) {
        super(posicion, nombre, id);
    }

    @Override
    public ArrayList<Posicion> ListarPosiblesMovimientos(Mapa mapa, byte filacomienzo) {
        ArrayList<Posicion> res = new ArrayList<>();
        AgregarPosicionesDependiendoDireccionDiagonal(0, res, mapa);
        AgregarPosicionesDependiendoDireccionDiagonal(1, res, mapa);
        AgregarPosicionesDependiendoDireccionDiagonal(2, res, mapa);
        AgregarPosicionesDependiendoDireccionDiagonal(3, res, mapa);

        return res;
    }
}
