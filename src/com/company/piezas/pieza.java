package com.company.piezas;

import com.company.herramientas.Posicion;
import com.company.mundo.Mapa;

import java.util.ArrayList;
import java.util.Map;

public interface pieza {
    public Posicion posicion();
    public ArrayList<Posicion> ListarPosiblesMovimientos(Mapa mapa, byte filacomienzo);
    public byte equipo();
    public void cambiarPosicion(Posicion posicion);
    public String NombrePieza();
}
