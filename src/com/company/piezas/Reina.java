package com.company.piezas;

import com.company.herramientas.Posicion;
import com.company.mundo.Mapa;

import java.util.ArrayList;

public class Reina implements pieza{

    public Reina(Posicion posicion, String nombre, byte id){
        _p = posicion;
        _nombre = nombre;
        _id = id;
    }


    @Override
    public Posicion posicion() {
        return _p;
    }

    @Override
    public ArrayList<Posicion> ListarPosiblesMovimientos(Mapa mapa, byte filacomienzo) {
        ArrayList<Posicion> res = new ArrayList<>();

        AgregarPosicionesDependiendoDireccionEnCruz(0, res, mapa);
        AgregarPosicionesDependiendoDireccionDiagonal(0, res, mapa);

        AgregarPosicionesDependiendoDireccionEnCruz(1, res, mapa);
        AgregarPosicionesDependiendoDireccionDiagonal(1, res, mapa);

        AgregarPosicionesDependiendoDireccionEnCruz(2, res, mapa);
        AgregarPosicionesDependiendoDireccionDiagonal(2, res, mapa);

        AgregarPosicionesDependiendoDireccionEnCruz(3, res, mapa);
        AgregarPosicionesDependiendoDireccionDiagonal(3, res, mapa);

        return res;
    }

    @Override
    public byte equipo() {
        return _id;
    }

    private boolean NoesteFueraDerango(Posicion posicion){
        boolean res = (posicion.y < 8 && posicion.y > -1) && (posicion.x < 8 && posicion.x > -1);
        return res;
    }

    public boolean noHayOtraPiezaAmiga(Posicion posicion, Mapa mapa){
        return mapa.noHayPiezaAmiga(posicion, _id);
    }

    public void AgregarPosicionesDependiendoDireccionEnCruz(int direccion, ArrayList<Posicion> direcciones, Mapa mapa){
        Posicion posicion = new Posicion(posicion().x, posicion().y);
        aumentarEnDireccionEnCruz(direccion, posicion);

        while (NoesteFueraDerango(posicion) && noHayOtraPiezaAmiga(posicion, mapa)){
            direcciones.add(new Posicion(posicion.x, posicion.y));
            if(mapa.hayPiezaEnemiga(posicion, _id)){
                break;
            }
            aumentarEnDireccionEnCruz(direccion, posicion);
        }
    }

    public void AgregarPosicionesDependiendoDireccionDiagonal(int direccion, ArrayList<Posicion> direcciones, Mapa mapa){
        Posicion posicion = new Posicion(posicion().x, posicion().y);
        aumentarEnDireccionEnDiagonal(direccion, posicion);

        while (NoesteFueraDerango(posicion) && noHayOtraPiezaAmiga(posicion, mapa)){
            direcciones.add(new Posicion(posicion.x, posicion.y));
            if(mapa.hayPiezaEnemiga(posicion, _id)){
                break;
            }
            aumentarEnDireccionEnDiagonal(direccion, posicion);
        }
    }

    public void aumentarEnDireccionEnDiagonal(int direccion, Posicion posicion){
        if(direccion == 0){
            posicion.y = posicion.y + 1;
            posicion.x = posicion.x - 1;
        }else {
            if(direccion == 1){
                posicion.x = posicion.x + 1;
                posicion.y = posicion.y + 1;
            }else{
                if(direccion == 2){
                    posicion.y = posicion.y - 1;
                    posicion.x = posicion.x + 1;
                }else{
                    posicion.x = posicion.x - 1;
                    posicion.y = posicion.y - 1;
                }
            }
        }
    }

    public void aumentarEnDireccionEnCruz(int direccion, Posicion posicion){
        if(direccion == 0){
            posicion.y = posicion.y + 1;
        }else {
            if(direccion == 1){
                posicion.x = posicion.x + 1;
            }else{
                if(direccion == 2){
                    posicion.y = posicion.y - 1;
                }else{
                    posicion.x = posicion.x - 1;
                }
            }
        }
    }

    @Override
    public void cambiarPosicion(Posicion posicion) {
        _p = posicion;
    }

    @Override
    public String NombrePieza() {
        return _nombre;
    }
    private String _nombre;

    /*-------------------------------*/
    private Posicion _p;
    byte _id;
}
