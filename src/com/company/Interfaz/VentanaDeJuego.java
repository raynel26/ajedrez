package com.company.Interfaz;

import com.company.Juego.Juego;
import com.company.herramientas.SetDePiezas;
import com.company.mundo.Mapa;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VentanaDeJuego extends JFrame  {

    public VentanaDeJuego() {
        this.setVisible(true);
        this.setSize(600, 600);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        _tablero = new TableroDeJuego();
        _fondo.setLayout(new BorderLayout());
        //ComenzarPartida.addActionListener(this);

        _fondo.add(_tablero, BorderLayout.CENTER);
        //_fondo.add(ComenzarPartida, BorderLayout.SOUTH);
        //_fondo.add(terminarPartida, BorderLayout.SOUTH);
        //_fondo.add(label, BorderLayout.NORTH);
        //_fondo.add(_jugadas, BorderLayout.EAST);


        this.add(_fondo);

        String b = "b";
        String n = "n";

        SetDePiezas blancasBack = new SetDePiezas("b");
        SetDePiezas negrasBack = new SetDePiezas("n");
        Mapa mapa = new Mapa(blancasBack, negrasBack);

        SetDibujoDePiezas blancos = new SetDibujoDePiezas(blancasBack);
        SetDibujoDePiezas negras = new SetDibujoDePiezas(negrasBack);

        iniciarPartida(blancos, negras, blancasBack, negrasBack, mapa);
    }


    public void iniciarPartida(SetDibujoDePiezas blancos, SetDibujoDePiezas negras, SetDePiezas b, SetDePiezas n, Mapa m) {
        _tablero.iniciarPartida(blancos, negras, b, n, m);
    }
/*
    public void ponerAescucharBlancas() {
        _tablero.blancasEscuchando();
    }

    public void ponerAescucharNegras() {
        _tablero.negrasEscuchando();
    }
*/

    /*
    @Override
    public void actionPerformed(ActionEvent e) {
        String b = "b";
        String n = "n";

        SetDePiezas blancasBack = new SetDePiezas("b");
        SetDePiezas negrasBack = new SetDePiezas("n");
        Mapa mapa = new Mapa(blancasBack, negrasBack);

        SetDibujoDePiezas blancos = new SetDibujoDePiezas(blancasBack);
        SetDibujoDePiezas negras = new SetDibujoDePiezas(negrasBack);

        iniciarPartida(blancos, negras, blancasBack, negrasBack, mapa);
    }
*/
    //-------------------------Estructura Interna---------------------------//

    //Partida _partida;
    private JList<String> _jugadas = new JList<>();
    private TableroDeJuego _tablero;
    private JPanel _fondo = new JPanel();
    private JButton ComenzarPartida = new JButton("Comenzar Partida");
    private  JButton terminarPartida = new JButton("Terminar Partida");
    private  JLabel label = new JLabel();

}