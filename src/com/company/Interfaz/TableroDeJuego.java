package com.company.Interfaz;

import com.company.Juego.Juego;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;


import com.company.herramientas.Posicion;
import com.company.herramientas.SetDePiezas;
import com.company.mundo.Mapa;
import com.company.piezas.pieza;


public class TableroDeJuego extends JPanel  {
    public TableroDeJuego() {
        this.setLayout(new GridLayout(8, 8));
        _tablero = new Cuadrilla[8][8];
        for (int j = 7; j > -1; j--) {
            Paint pintar;
            if (j % 2 != 0) {
                pintar = Color.GRAY;
            } else {
                pintar = Color.LIGHT_GRAY;
            }
            for (int i = 0; i < 8; i++) {
                _tablero[i][j] = new Cuadrilla(pintar, new Posicion(i, j));
                add(_tablero[i][j]);
                if (pintar == Color.GRAY) {
                    pintar = Color.LIGHT_GRAY;
                } else {
                    pintar = Color.GRAY;
                }
            }
        }
    }

    public void iniciarPartida(SetDibujoDePiezas blancos, SetDibujoDePiezas negras, SetDePiezas b, SetDePiezas n, Mapa mapa) {
        _set = n;
        //ubico piezas blancas en el tablero
        LinkedList<Tupla> listaDePiezasBlancas = blancos.get_p();
        ListIterator it1 = listaDePiezasBlancas.listIterator();
        while (it1.hasNext()){
            Tupla actual = (Tupla) it1.next();
            int x = actual.get_p().posicion().x;
            int y = actual.get_p().posicion().y;
            _tablero[x][y].setPieza(actual);
        }

        //ubico piezas negras en el tablero
        LinkedList<Tupla> listaDePiezasNegras = negras.get_p();
        ListIterator it2 = listaDePiezasNegras.listIterator();
        while (it2.hasNext()){
            Tupla actual = (Tupla) it2.next();
            int x = actual.get_p().posicion().x;
            int y = actual.get_p().posicion().y;
            _tablero[x][y].setPieza(actual);
        }

        _juego = new Juego(b, n);
        _turno = "b";
        this.blancasEscuchando();
        System.out.println(_juego.enTurno());

    }

    public void blancasEscuchando() {
        LinkedList<pieza> piezas =  _juego.piezasBlancasVivas();
        ListIterator it = piezas.listIterator();

        while (it.hasNext()){
            pieza actual = (pieza) it.next();
            ArrayList<Posicion> posiblesMovimientos = _juego.SeleccionarPieza(actual);
            if(posiblesMovimientos.size() > 0){
                int x = actual.posicion().x;
                int y = actual.posicion().y;
                ponerAlaEscuchaPosicionConPieza(_tablero[x][y]);
            }
        }
    }

    public void dejarDeEscucharABlancos(){
        LinkedList<pieza> piezas =  _juego.piezasBlancasVivas();
        ListIterator it = piezas.listIterator();

        while (it.hasNext()){
            pieza actual = (pieza) it.next();
            int x = actual.posicion().x;
            int y = actual.posicion().y;
            quitarDelaEscuchaAposicionConPieza(_tablero[x][y]);
        }
    }

    public void negrasEscuchando() {
        LinkedList<pieza> piezas =  _juego.piezasNegrasVivas();
        ListIterator it = piezas.listIterator();

        while (it.hasNext()){
            pieza actual = (pieza) it.next();
            ArrayList<Posicion> posiblesMovimientos = _juego.SeleccionarPieza(actual);
            if(posiblesMovimientos.size() > 0){
                int x = actual.posicion().x;
                int y = actual.posicion().y;
                ponerAlaEscuchaPosicionConPieza(_tablero[x][y]);
            }
        }
    }

    public void dejarDeEscucharANegros(){
        LinkedList<pieza> piezas =  _juego.piezasNegrasVivas();
        ListIterator it = piezas.listIterator();

        while (it.hasNext()){
            pieza actual = (pieza) it.next();
            ArrayList<Posicion> posiblesMovimientos = _juego.SeleccionarPieza(actual);
            if(posiblesMovimientos.size() > 0){
                int x = actual.posicion().x;
                int y = actual.posicion().y;
                quitarDelaEscuchaAposicionConPieza(_tablero[x][y]);
            }
        }
    }


    public void quitarDelaEscuchaAposicionConPieza(Cuadrilla c){
        pieza p =  c.get_pieza().get_p();
        ArrayList<Posicion> posiblesMovimientos = _juego.SeleccionarPieza(p);
        for (int i = 0; i < posiblesMovimientos.size(); i++){
            int x = posiblesMovimientos.get(i).x;
            int y =  posiblesMovimientos.get(i).y;
            c.removeMouseListener(_tablero[x][y]);
        }
        //c.setBackground((Color) c.get_color());
        c.removeMouseListener(c);
    }

    public void ponerAlaEscuchaPosicionConPieza(Cuadrilla c){
        pieza p =  c.get_pieza().get_p();
        ArrayList<Posicion> posiblesMovimientos = _juego.SeleccionarPieza(p);
        if(posiblesMovimientos.size() > 0){
            //c.setBackground(Color.GREEN);
            c.addMouseListener(c);
            for (int i = 0; i < posiblesMovimientos.size(); i++){
                Posicion posicionActual = new Posicion(posiblesMovimientos.get(i).x, posiblesMovimientos.get(i).y);
                c.addMouseListener(_tablero[posicionActual.x][posicionActual.y]);
            }
        }

    }

    public Boolean noEstaSeleccionado(Cuadrilla c){
        return c != _piezaSeleccionada;
    }

    public void seleccionar(Cuadrilla cuadrillaAseleccionar){
        cuadrillaAseleccionar.setBackground(Color.CYAN);
        pieza piezaEnposicion = cuadrillaAseleccionar.get_pieza().get_p();

        ArrayList<Posicion> posiblesMovimientos = _juego.SeleccionarPieza(piezaEnposicion);
        for(int i = 0; i < posiblesMovimientos.size(); i++){
            int x1 = posiblesMovimientos.get(i).x;
            int y1 = posiblesMovimientos.get(i).y;
            for(int j = 0; j < posiblesMovimientos.size(); j++){
                int x2 = posiblesMovimientos.get(j).x;
                int y2 = posiblesMovimientos.get(j).y;
                _tablero[x1][y1].addMouseListener(_tablero[x2][y2]);
            }
            _tablero[x1][y1].setBackground(Color.ORANGE);
        }
    }

    public void desSeleccionar(Cuadrilla cuadrillaADesseleccionar){
        cuadrillaADesseleccionar.setBackground((Color) cuadrillaADesseleccionar.get_color());
        pieza piezaEnposicion = cuadrillaADesseleccionar.get_pieza().get_p();

        ArrayList<Posicion> posiblesMovimientos = _juego.SeleccionarPieza(piezaEnposicion);
        for(int i = 0; i < posiblesMovimientos.size(); i++){
            int x1 = posiblesMovimientos.get(i).x;
            int y1 = posiblesMovimientos.get(i).y;
            for(int j = 0; j < posiblesMovimientos.size(); j++){
                int x2 = posiblesMovimientos.get(j).x;
                int y2 = posiblesMovimientos.get(j).y;
                _tablero[x1][y1].removeMouseListener(_tablero[x2][y2]);
            }
            _tablero[x1][y1].setBackground((Color) _tablero[x1][y1].get_color());
        }
    }

    public void cambiarTurno(){
        if(_juego.enTurno().equals("b")){
            dejarDeEscucharABlancos();
            negrasEscuchando();
        }else{
            dejarDeEscucharANegros();
            blancasEscuchando();
        }
        _juego.CambiarTurno();
    }

    public void moverPieza(Cuadrilla c){
        c.setPieza(_piezaSeleccionada._pieza);
        desSeleccionar(_piezaSeleccionada);
        if(_juego.enTurno() == "b"){
            dejarDeEscucharABlancos();
            _juego.MoverBlanco(_piezaSeleccionada.get_pieza().get_p(), c.get_posicion());
            negrasEscuchando();
        }else{
            dejarDeEscucharANegros();
            _juego.MoverNegro(_piezaSeleccionada.get_pieza().get_p(), c.get_posicion());
            blancasEscuchando();
        }
        _piezaSeleccionada.deletePieza();
        _juego.CambiarTurno();
        _juego.tablero();
        for(int j = 0; j < _juego.SeleccionarPieza(_set.piezas().get("rey")).size(); j++){
            System.out.print(_juego.SeleccionarPieza(_set.piezas().get("rey")).get(j) + " ");
        }
        System.out.println();
    }

    public boolean estaEntreLasPosicionesDeLaPiezaSeleccionada(Cuadrilla c){
        ArrayList<Posicion> posiciones = _juego.SeleccionarPiezaa(_piezaSeleccionada._pieza.get_p());
        int i = 0;
        while (i < posiciones.size() && posiciones.get(i) != c._posicion){
            i++;
        }
        return i < posiciones.size();
    }

    public Boolean haySeleccionada(){
        return _piezaSeleccionada != null;
    }


    //-------------------------Estructura Interna---------------------------//

    class Cuadrilla extends JPanel implements MouseListener {
        public Cuadrilla(Paint color, Posicion posicion) {
            this.setBackground((Color) color);
            this.setVisible(true);
            //this.setLayout(new BorderLayout());
            _color = color;
            _posicion = posicion;
        }
        public void  colorear(){
            this.setBackground(Color.GREEN);
        }

        public void setPieza(Tupla pieza) {
            _pieza = pieza;
            repaint();
        }

        public void deletePieza() {
            _pieza = null;
            repaint();
        }

        public void paint(Graphics g) {
            super.paint(g);
            g.setFont(_fuente);
            if(_pieza != null){
                g.drawString(_pieza.get_representacion(), 15, 55);
            }else{
                g.drawString("", 0, 0);
            }
        }

        public Tupla get_pieza () {
               return _pieza;
        }

        public void set_pieza (Tupla _pieza){
            this._pieza = _pieza;
        }

        public Paint get_color() {
            return _color;
        }



        public Posicion get_posicion() {
            return _posicion;
        }

        public boolean piezaSeleccionada(MouseEvent e){
            Cuadrilla evento = (Cuadrilla) e.getSource();
            boolean hayPiezaDesdeElAccionador = evento._pieza != null;
            return hayPiezaDesdeElAccionador && this != evento && this.getBackground() == this.get_color();
        }

        @Override
        public void mouseClicked (MouseEvent e) {
            Cuadrilla evento = (Cuadrilla) e.getSource();
            boolean hayPieza = evento._pieza != null;
            boolean estaSeleccionado = !noEstaSeleccionado(this);

            if(evento == this){
               if(!haySeleccionada() && hayPieza && this.getBackground() != Color.ORANGE){
                   seleccionar(this);
                   _piezaSeleccionada = this;
               }
               if(haySeleccionada() && hayPieza && estaSeleccionado && this.getBackground() != Color.ORANGE){
                   desSeleccionar(this);
                   _piezaSeleccionada = null;
               }
               if(haySeleccionada() && hayPieza && !estaSeleccionado && this.getBackground() != Color.ORANGE){
                   desSeleccionar(_piezaSeleccionada);
                   _piezaSeleccionada = null;
                   seleccionar(this);
                   _piezaSeleccionada = this;
               }
               if(haySeleccionada() && hayPieza && this.getBackground() == Color.ORANGE){
                   moverPieza(this);
                   _piezaSeleccionada = null;
               }
               if(haySeleccionada() && !hayPieza && this.getBackground() == Color.ORANGE){
                   moverPieza(this);
                   _piezaSeleccionada = null;
               }
            }

        }

        @Override
        public void mousePressed (MouseEvent e){
        }

        @Override
        public void mouseReleased (MouseEvent e){
        }

        @Override
        public void mouseEntered (MouseEvent e){
        }

        @Override
        public void mouseExited (MouseEvent e){
        }
            //-----------------------------------------------------//
            private Posicion _posicion;
            private Paint _color;
            private Tupla _pieza;
            private Font _fuente = new Font("Chess Cases", Font.TRUETYPE_FONT, 46);
    }
    private Cuadrilla _piezaSeleccionada = null;
    private String _turno;
    private Juego _juego;
    private Cuadrilla[][] _tablero;
    private SetDePiezas _set;
}