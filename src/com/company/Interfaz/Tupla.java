package com.company.Interfaz;

import com.company.piezas.pieza;

public class Tupla {

    Tupla(String representacion, pieza p){
        _representacion = representacion;
        _p = p;
    }

    public pieza get_p() {
        return _p;
    }

    public String get_representacion() {
        return _representacion;
    }

    private String _representacion;
    private pieza _p;
}
