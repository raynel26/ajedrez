package com.company.Interfaz;

import com.company.herramientas.SetDePiezas;
import com.company.piezas.pieza;

import java.awt.*;
import java.util.HashMap;
import java.util.LinkedList;

public class SetDibujoDePiezas {

    public SetDibujoDePiezas(SetDePiezas piezas) {
        _piezas = new HashMap<>();
        _p = new LinkedList<>();
        HashMap<String, String> dibujosDepiezas = new HashMap<>();
        LinkedList<pieza>  listaPiezas = piezas.get_piezasVivas();
        if (piezas.get_colorJugador() == "b") {

            for (int i = 0; i < listaPiezas.size(); i++){
                if(listaPiezas.get(i).NombrePieza() == "torre"){
                    _p.add(new Tupla("r", listaPiezas.get(i)));
                }else {
                    if(listaPiezas.get(i).NombrePieza() == "alfil"){
                        _p.add(new Tupla("b", listaPiezas.get(i)));
                    }else {
                        if (listaPiezas.get(i).NombrePieza() == "caballo") {
                            _p.add(new Tupla("n", listaPiezas.get(i)));
                        }else {
                            if (listaPiezas.get(i).NombrePieza() == "rey") {
                                _p.add(new Tupla("q", listaPiezas.get(i)));
                            }else {
                                if (listaPiezas.get(i).NombrePieza() == "reina") {
                                    _p.add(new Tupla("k", listaPiezas.get(i)));
                                }else {
                                    _p.add(new Tupla("p", listaPiezas.get(i)));
                                }
                            }
                        }

                    }

                }


            }
        } else {

            for (int i = 0; i < listaPiezas.size(); i++){
                if(listaPiezas.get(i).NombrePieza() == "torre"){
                    _p.add(new Tupla("t", listaPiezas.get(i)));
                }else {
                    if(listaPiezas.get(i).NombrePieza() == "alfil"){
                        _p.add(new Tupla("v", listaPiezas.get(i)));
                    }else {
                        if (listaPiezas.get(i).NombrePieza() == "caballo") {
                            _p.add(new Tupla("m", listaPiezas.get(i)));
                        }else {
                            if (listaPiezas.get(i).NombrePieza() == "rey") {
                                _p.add(new Tupla("l", listaPiezas.get(i)));
                            }else {
                                if (listaPiezas.get(i).NombrePieza() == "reina") {
                                    _p.add(new Tupla("w", listaPiezas.get(i)));
                                }else {
                                    _p.add(new Tupla("o", listaPiezas.get(i)));
                                }
                            }
                        }
                    }
                }
            }

            /*String torre1 = "t";
            String torre2 = "t";
            String alfil1 = "v";
            String alfil2 = "v";
            String caballo1 = "m";
            String caballo2 = "m";
            String rey = "w";
            String reina = "l";
            String peon1 = "o";
            String peon2 = "o";
            String peon3 = "o";
            String peon4 = "o";
            String peon5 = "o";
            String peon6 = "o";
            String peon7 = "o";
            String peon8 = "o";
            _piezas.put("torre1", new Tupla(torre1, pi.get("torre1")));
            _piezas.put("torre2", new Tupla(torre2, pi.get("torre2")));
            _piezas.put("alfil1", new Tupla(alfil1, pi.get("alfil1")));
            _piezas.put("alfil2", new Tupla(alfil2, pi.get("alfil2")));
            _piezas.put("caballo1", new Tupla(caballo1, pi.get("caballo1")));
            _piezas.put("caballo2", new Tupla(caballo2, pi.get("caballo2")));
            _piezas.put("rey", new Tupla(rey, pi.get("rey")));
            _piezas.put("reina", new Tupla(reina, pi.get("reina")));
            _piezas.put("peon1", new Tupla(peon1, pi.get("peon1")));
            _piezas.put("peon2", new Tupla(peon2, pi.get("peon2")));
            _piezas.put("peon3", new Tupla(peon3, pi.get("peon3")));
            _piezas.put("peon4", new Tupla(peon4, pi.get("peon4")));
            _piezas.put("peon5", new Tupla(peon5, pi.get("peon5")));
            _piezas.put("peon6", new Tupla(peon6, pi.get("peon6")));
            _piezas.put("peon7", new Tupla(peon7, pi.get("peon7")));
            _piezas.put("peon8", new Tupla(peon8, pi.get("peon8")));*/
        }

    }


    public HashMap<String, Tupla> get_piezas() {
        return _piezas;
    }

    public LinkedList<Tupla> get_p() {
        return _p;
    }

    //------------------------------------------------------------------//
    private HashMap<String, Tupla> _piezas;
    private LinkedList<Tupla> _p;
}
