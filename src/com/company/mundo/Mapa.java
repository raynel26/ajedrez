package com.company.mundo;

import com.company.herramientas.Posicion;
import com.company.herramientas.SetDePiezas;
import com.company.piezas.pieza;

import java.util.HashMap;
import java.util.LinkedList;

public class Mapa {

    public Mapa(SetDePiezas blancas, SetDePiezas negras){
        _tablero = new pieza[8][8];
        //HashMap<String, pieza> piezasBlancas = blancas.piezas();

        LinkedList<pieza> piezasB = blancas.get_piezasVivas();
        for (int i = 0; i < piezasB.size(); i++){
            int x = piezasB.get(i).posicion().x;
            int y = piezasB.get(i).posicion().y;
            _tablero[x][y] = piezasB.get(i);
        }

        LinkedList<pieza> piezasN = negras.get_piezasVivas();
        for (int i = 0; i < piezasN.size(); i++){
            int x = piezasN.get(i).posicion().x;
            int y = piezasN.get(i).posicion().y;
            _tablero[x][y] = piezasN.get(i);
        }

    }

    public void MoverPieza(pieza pieza, Posicion p){
        _tablero[pieza.posicion().x][pieza.posicion().y] = null;
        _tablero[p.x][p.y] = pieza;
    }

    public pieza HayPiezaAhi(Posicion posicion, byte equipo){
        pieza p = null;
        if(_tablero[posicion.x][posicion.y] != null && _tablero[posicion.x][posicion.y].equipo() == equipo){
            p= _tablero[posicion.x][posicion.y];
        }
        return p;
    }

    public  void revivirPieza(pieza pieza, Posicion posicion){
        _tablero[posicion.x][posicion.y] = pieza;
    }

    public boolean filaContraria(pieza pieza, byte filaComienzo){
        boolean res = false;
        if(filaComienzo == 1 && pieza.posicion(). y == 7){
            res = true;
        }else {
            if(filaComienzo == 6 && pieza.posicion(). y == 0){
                res = false;
            }
        }
        return res;
    }

    public void matarPieza(pieza piezaAmatar, pieza piezaAmover){
        int x = piezaAmatar.posicion().x;
        int y = piezaAmatar.posicion().y;
        _tablero[x][y] = piezaAmover;
    }

    public boolean noHayPiezaAmiga(Posicion posicion, byte equipo){
        return _tablero[posicion.x][posicion.y] == null || _tablero[posicion.x][posicion.y].equipo() != equipo;
    }

    public boolean hayPiezaEnemiga(Posicion posicion, byte equipo){
        return _tablero[posicion.x][posicion.y] != null && _tablero[posicion.x][posicion.y].equipo() != equipo;
    }

    public void piezasJugador(){

    }


    public void tablero(){
        int j = 7;
        while (j > -1){
            int i = 0;
            while (i < 8){
                if (_tablero[i][j] == null){
                    System.out.print("*" + "    ");
                }else {
                    System.out.print(_tablero[i][j].NombrePieza() + " ");
                }
                i++;
            }
            System.out.println();
            j--;
        }
    }

    //*---------------------------*//
    private pieza _tablero[][];
}
